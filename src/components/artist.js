import React from 'react';
import Albumsofartist from "../pages/albumsofartist/albumsofartist";

const Artists = ({ artist }) => {
    return (
      <div class="artist">
        {artist.map((artist) => (
          <div class="card">
            <div class="card-body">
              <table>
                <thread>
                  <tr>
                    <th colspan="1">Artiste</th>
                    <th colspan="1">Avatar</th>
                  </tr>
                </thread>
                <tbody>
                  <tr>
                    <td>{artist.alias}</td>
                    <td><img className='avatar' src='https://i.pravatar.cc/150/'></img></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        ))}

      </div>
    )
  };

  export default Artists