import React from 'react';

const AlbumsofArtists = ({ albums }) => {
    return (
      <div class="albums">
        <center><h1>List albums</h1></center>
        {albums.map((album) => (
          <div class="card">
            <div class="card-body">
              <h5>{album.name}</h5>
            </div>
          </div>
        ))}
      </div>
    )
  };
 export default AlbumsofArtists