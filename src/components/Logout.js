import React from 'react';
import {useAuth} from "../context/auth";
import {Link} from "react-router-dom";
import {expires} from "../App";
import Nav from "react-bootstrap/cjs/Nav";
import Button from "react-bootstrap/cjs/Button";

function Logout() {
    const { setAuthTokens } = useAuth();

    function logout() {
        setAuthTokens();
    }

    if(new Date().getTime() > parseInt(expires)) {
        logout();
    }

    return <Button variant="primary" onClick={logout}>Déconnexion</Button>
}

export default Logout