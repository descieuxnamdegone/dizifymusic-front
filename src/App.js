import './App.css';
import React, {useState } from 'react';
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import { AuthContext } from "./context/auth";
import Library from './pages/library/library';
import Album from './pages/album/album';
import Artist from './pages/artist/artist';
import Home from './pages/home/Home';
import PrivateRoute from "./PrivateRoute";
import Login from "./pages/login/Login";
import Signup from "./pages/signup/Signup";
import Albumsofartist from "./pages/artist/albumsofartist";
import Logout from "./components/Logout";
import Playlist from "./pages/playlist/Playlist";
import TitleOfAlbum from "./pages/titleOfAlbum/titleOfAlbum";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "react-bootstrap/cjs/Navbar";
import Nav from "react-bootstrap/cjs/Nav";
import EditPlaylist from "./pages/playlist/EditPlaylist";

export const token = localStorage.getItem("token") ? JSON.parse(localStorage.getItem("token")).token : '';
export const expires = localStorage.getItem("token") ? JSON.parse(localStorage.getItem("token")).expires : '';
export const userId = localStorage.getItem("token") ? JSON.parse(localStorage.getItem("token")).userId : '';

function App() {
  const [authTokens, setAuthTokens] = useState(token);
  const setTokens = (data) => {
    if(data === undefined) {
      localStorage.removeItem('token');
      setAuthTokens(data);
      return;
    }
    localStorage.setItem("token", JSON.stringify(data));
    setAuthTokens(data);
  }

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Router>

        <Navbar bg="dark" variant="dark" className="mb-3">
          <Navbar.Brand href="/">Dizify</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="/library">Bibliothèque</Nav.Link>
            <Nav.Link href="/album">Album</Nav.Link>
            <Nav.Link href="/artist">Artist</Nav.Link>
            <Nav.Link href="/playlist">Mes playlists</Nav.Link>
            {!authTokens ? <Nav.Link href="/login">Connexion</Nav.Link> : ''}
            {!authTokens ? <Nav.Link href="/signup">Inscription</Nav.Link> : ''}
          </Nav>
          {authTokens ? <Logout></Logout> : ''}
        </Navbar>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={Signup} />
        <PrivateRoute path="/library" component={Library} />
        <PrivateRoute exact path="/album" component={Album}/>
         <PrivateRoute exact path="/artist" component={Artist}/>
         <PrivateRoute exact path="/playlist" component={Playlist}/>
         <PrivateRoute path="/playlist/:id" component={EditPlaylist}/>
         <PrivateRoute path="/album/:id/title" component={TitleOfAlbum}/>
         <PrivateRoute path="/artist/:id/album" component={Albumsofartist} />

        </Router>
    </AuthContext.Provider>
  );
}
export default App;