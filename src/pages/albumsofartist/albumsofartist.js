//import './album.css';
import React, { Component } from 'react';
//import Artists from '../../components/artist';
import {token} from "../../App";
import axios from "axios";
import Button from "react-bootstrap/cjs/Button";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import Table from "react-bootstrap/cjs/Table";
import Col from "react-bootstrap/cjs/Col";
import Container from "react-bootstrap/cjs/Container";
import Row from "react-bootstrap/cjs/Row";


class Albumsofartist extends Component {
     constructor(props) {
            super(props);
            this.state = {
                artist: {
                    album:[]
                }
            };


  this.id = this.props.match.params.id
}
  render() {
   return (
              <>
                  <Container>
                      <Row>
                          <Col>
                              <Table striped bordered hover>
                                  <thead>
                                      <tr>
                                          <th>Les album de l artiste</th>
                                          <th>Nom</th>
                                          <th>Date</th>
                                          <th>Image</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      {this.state.artist.album((album) => {
                                          return <tr>
                                              <td>{album.name}</td>
                                              <td>{album.date}</td>
                                              <td>{album.image}</td>
                                          </tr>
                                      })}
                                  </tbody>
                              </Table>
                          </Col>
                      </Row>
                  </Container>
              </>
          )

  }


    componentDidMount() {
     axios.get('http://localhost:8080/artist/'+this.id+'/album', {
       headers: {
         'Authorization': token,
         'Accept'       : 'application/json',
         'Content-Type' : 'application/json',
       },
     })
     .then(result => {
           if (result.status === 200) {
               this.setState({album: result.data});
           } else {
          }
       }).catch(e => {

      });
}
}

export default Albumsofartist;
