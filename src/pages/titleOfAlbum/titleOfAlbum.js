import React, { Component } from 'react';
import {token} from "../../App";

import Table from "react-bootstrap/cjs/Table";
import Col from "react-bootstrap/cjs/Col";
import Container from "react-bootstrap/cjs/Container";
import Row from "react-bootstrap/cjs/Row";
import axios from 'axios';

class TitleOfAlbum extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: []
        };
        this.id = this.props.match.params.id
    }

    render() {
        return (
            <>
                <Container>
                    <Row>
                        <Col>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Titre de la chanson</th>
                                        <th>Durée</th>
                                        <th>Artiste</th>
                                        <th>Album</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.title.map((title) => {
                                        return <tr>
                                            <td>{title.name}</td>
                                            <td>{title.duration}</td>
                                            <td>{title.artist.alias}</td>
                                            <td>{title.album.name}</td>
                                        </tr>
                                    })}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }

    componentDidMount() {
        axios.get("http://localhost:8080/album/" + this.id + "/title/",
            {'headers': {'Authorization': token}}).then(result => {
            if (result.status === 200) {
                this.setState({title: result.data})
            } else {

            }
        }).catch(e => {

        });
    }
}

export default TitleOfAlbum;