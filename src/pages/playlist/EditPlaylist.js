import React, {useState} from "react";
import Table from "react-bootstrap/cjs/Table";
import Col from "react-bootstrap/cjs/Col";
import Container from "react-bootstrap/cjs/Container";
import Row from "react-bootstrap/cjs/Row";
import Button from "react-bootstrap/cjs/Button";
import Modal from "react-bootstrap/cjs/Modal";
import axios from "axios";
import {token, userId} from "../../App";
import {Error, Form, Input} from "../../components/AuthForm";
import Alert from "react-bootstrap/cjs/Alert";

class EditPlaylist extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            playlist: {
                title: []
            }
        };
        this.id = this.props.match.params.id
    }

    componentDidMount() {
        axios.get("http://localhost:8080/playlist/" + this.id,
            {'headers': {'Authorization': token}}).then(result => {
            if (result.status === 200) {
                this.setState({playlist: result.data})
            } else {

            }
        }).catch(e => {

        });
    }

    render() {
        return (
            <>
                <Container>
                    <Row>
                        <Col>
                            <Table striped bordered hover>
                                <thead>
                                <tr>
                                    <th>Titre</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.playlist.title.map((title) => {
                                    return <tr>
                                        <td>{title.name}</td>
                                        <td><Button variant="danger" onClick={this.deleteTitle.bind(this, title)}>Supprimer</Button></td>
                                    </tr>
                                })}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }

    deleteTitle(title) {
        var playlist = this.state.playlist;
        const titles = this.state.playlist.title.filter(i => i.id !== title.id);
        playlist.title = titles;
        axios.put("http://localhost:8080/playlist/" +  this.id, playlist,
            {'headers': {'Authorization': token}}).then(result => {
            if (result.status === 200) {
                this.setState({playlist: playlist});
            } else {
            }
        }).catch(e => {
        });
    }
}

export default EditPlaylist