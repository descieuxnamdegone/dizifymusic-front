import React, {useState} from "react";
import Table from "react-bootstrap/cjs/Table";
import Col from "react-bootstrap/cjs/Col";
import Container from "react-bootstrap/cjs/Container";
import Row from "react-bootstrap/cjs/Row";
import Button from "react-bootstrap/cjs/Button";
import Modal from "react-bootstrap/cjs/Modal";
import axios from "axios";
import {token, userId} from "../../App";
import {Error, Form, Input} from "../../components/AuthForm";
import Alert from "react-bootstrap/cjs/Alert";
import {Link} from "react-router-dom";

class Playlist extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            playlists: [],
            show: false,
            name: "",
            created: false,
            isError: false
        };
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    componentDidMount() {
        axios.get("http://localhost:8080/user/" + userId + "/playlist",
            {'headers': {'Authorization': token}}).then(result => {
            if (result.status === 200) {
                this.setState({playlists: result.data})
            } else {

            }
        }).catch(e => {

        });
    }

    render() {
        return (
            <>
                <Container>
                    <Row>
                        <Col>
                            <Button variant="primary" onClick={this.handleShow} className="mb-4 float-right">
                                Ajouter une playlist
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Table striped bordered hover>
                                <thead>
                                <tr>
                                    <th>Nom de la plalist</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.playlists.map((playlist) => {
                                    return <tr>
                                        <td>{playlist.name}</td>
                                        <td>
                                            <Link to={'/playlist/' + playlist.id}><Button variant="success" className="mr-3">Éditer</Button></Link>
                                            <Button variant="danger" onClick={this.deletePlaylist.bind(this, playlist)}>Supprimer</Button>
                                        </td>
                                    </tr>
                                })}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>Ajouter une playlist</Modal.Header>
                    <Modal.Body>
                            <>
                                <Form>
                                    <label>Nom</label>
                                    <Input
                                        type="text"
                                        value={this.state.name}
                                        onChange={e => {
                                            this.setState({name: e.target.value})
                                        }}
                                    />
                                </Form>
                                { this.state.isError &&<Error>Erreur</Error> }
                                <Modal.Footer><Button variant="primary" onClick={this.postPlaylist.bind(this)}>
                                    Enregistrer
                                </Button></Modal.Footer>
                            </>
                    </Modal.Body>
                </Modal>
            </>
        )
    }

    handleClose () {
        this.setState({show: false});
    }

    handleShow () {
        this.setState({show: true});
    }

    postPlaylist() {
        axios.post("http://localhost:8080/playlist", {
            'name': this.state.name,
            'user': {
                'id': userId
            }
        }, {'headers': {'Authorization': token}}).then(result => {
            if (result.status === 200) {
                this.setState({show: false});
                const playlists = this.state.playlists.concat(result.data);
                this.setState({playlists: playlists});
            } else {
                this.setState({isError: true});
            }
        }).catch(e => {
            this.setState({isError: true});
        });
    }

    deletePlaylist(playlist) {
        axios.delete("http://localhost:8080/playlist/" + playlist.id,
            {'headers': {'Authorization': token}}).then(result => {
            if (result.status === 200) {
                const playlists = this.state.playlists.filter(i => i.id !== playlist.id);
                this.setState({playlists: playlists});
            } else {
            }
        }).catch(e => {
        });
    }
}

export default Playlist