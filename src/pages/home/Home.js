import axios from 'axios';
import './home.css';
import React, { Component } from "react";
/*
import Titles from '../../components/title';
import Albums from '../../components/album';
import Artists from '../../components/artist';*/

class Home extends Component {

    state = {
        title: []
    }

    state = {
        album: []
    }
  
    getTitle() {
        axios.get('http://localhost:8080/title', { 
            headers: {
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA2NjQwOTQzfQ.YOhhIyXHnRJmRlh8stitaO63hxqoflWO0QFRZTK7l4iibFbTJGntBCkLpobGiSeF_W5voaTKoZE1m9AAyOUxkw',
                'Accept'       : 'application/json',
                'Content-Type' : 'application/json',
            }
        })
        .then(res => {
            const title = res.data;
            this.setState({title});
        })
    }

    getAlbum() {
        axios.get('http://localhost:8080/album', {
            headers: {
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA2NjQwOTQzfQ.YOhhIyXHnRJmRlh8stitaO63hxqoflWO0QFRZTK7l4iibFbTJGntBCkLpobGiSeF_W5voaTKoZE1m9AAyOUxkw',
                'Accept'       : 'application/json',
                'Content-Type' : 'application/json',
            }
        })
        .then(res => {
            const album = res.data;
            this.setState({album});
        })
    }
    
    getArtist() {
        axios.get('http://localhost:8080/artist', {
            headers: {
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA2NjQwOTQzfQ.YOhhIyXHnRJmRlh8stitaO63hxqoflWO0QFRZTK7l4iibFbTJGntBCkLpobGiSeF_W5voaTKoZE1m9AAyOUxkw',
                'Accept'       : 'application/json',
                'Content-Type' : 'application/json',
            }
        })
        .then(res => {
            const artist = res.data;
            this.setState({artist});
        })            
    }

    render() {
        return (
            <div className='homepage'>
                <h1 className='homeTitle'>Bienvenue sur Dizify</h1>

                <h2 className='tendanceTitle'>Tendances</h2>

                <div className='tendances'>
                    <div className='dailyArtist'>
                        <h3 className='nameArtist'>Un artiste</h3>
                        <img src='https://i.pravatar.cc/200/'/>
                    </div>
                    <div className='dailyAlbum'>
                        <h3 className='albumTitle'>Un album</h3>
                        <img src='https://picsum.photos/200/'/>
                    </div>
                    <div className='dailyTitle'>
                        <h3 className='titleMusic'>Un titre</h3>
                        <h4>Un artiste</h4>
                        <img src='https://i.pravatar.cc/150/'/>
                    </div>
                </div>

                <div className='resume'>
                    <h3>Qu'est ce que Dizify ?</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div>
            </div>     
        );
    }

}

/*
    function getTitle() {
        axios.get('http://localhost:8080/title', {
                "name" : name
            }).then(result => {
                if (result.status === 200) {
                    
                } else {
                    setIsError(true);
                }
            }).catch(e => {
                setIsError(true);
            });
    }
/*
    getAlbum() {
        axios.get('http://localhost:8080/album', {
            "name" : name,
            "image" : cover
        }).then(result => {
            if(result.status==200) {

            } else {
                setIsError(true);
            }
        }).catch(e => {
            setIsError(true);
        });
    }

    getArtist() {
        axios.get('http://localhost:8080/artist', {
            "alias" : alias,
            "image" : avatar
        }).then(result => {
            if(result.status==200) {

            } else {
                setIsError(true);
            }
        }).catch(e => {
            setIsError(true);
        });
    }

    */


export default Home;