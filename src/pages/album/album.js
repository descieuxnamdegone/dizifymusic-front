import React, { Component } from 'react';
import {token} from "../../App";
import Table from "react-bootstrap/cjs/Table";
import Col from "react-bootstrap/cjs/Col";
import Container from "react-bootstrap/cjs/Container";
import Row from "react-bootstrap/cjs/Row";
import {Link} from "react-router-dom";

class Album extends Component {

  state = {
    album: []
  }

  render() {
    return (
      <>
        <Container>
          <Row>
            <Col>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Artiste</th>
                    <th>Nom de l'album</th>
                    <th>Couverture</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.album.map((album) => {
                    return <tr>
                      <td>{album.artist && album.artist.alias}</td>
                      <td><Link to={'/album/' + album.id + '/title'}>{album.name}</Link></td>
                      <td><img className='cover' src='https://picsum.photos/150/'></img></td>
                    </tr>
                  })}
                </tbody>
              </Table>
            </Col>
          </Row>
        </Container>
      </>
    );
  }

  componentDidMount() {
    fetch('http://localhost:8080/album', {
      headers: {
        'Authorization': token,
        'Accept'       : 'application/json',
        'Content-Type' : 'application/json',
      },
    })
    .then(res => res.json())
    .then((data) => {
      this.setState({ album: data })
    })
    .catch(console.log)
  }
}

export default Album;
