//import './album.css';
import React, { Component } from 'react';
//import Artists from '../../components/artist';
import {token} from "../../App";
import axios from "axios";
import Button from "react-bootstrap/cjs/Button";
import Albumsofartist from "../albumsofartist/albumsofartist";
import {Link} from "react-router-dom";


class Artist extends Component {

  state = {
    artist: [],
     }

  render() {
   return (
         <div class="artist">
           {this.state.artist.map((artist) => (
             <div class="card">
               <div class="card-body">
                 <h5 class="card-artist">{artist.alias}</h5>
                 <img className='avatar' src='https://i.pravatar.cc/150/'></img>
               </div>
               <div>
                <Link to={'artist/' + artist.id + '/album'} ><Button variant="primary" className="mb-4 float-right">
                                Afficher ma discographie
                </Button></Link>
               </div>
             </div>

           ))}

         </div>
       )

  }

  componentDidMount() {
    axios.get('http://localhost:8080/artist', {
      headers: {
        'Authorization': token,
        'Accept'       : 'application/json',
        'Content-Type' : 'application/json',
      },
    })
    .then(result => {
          if (result.status === 200) {
              this.setState({artist: result.data})
          } else {

          }
      }).catch(e => {

      });
  }

  showAlbums(){

  }
}

export default Artist;
