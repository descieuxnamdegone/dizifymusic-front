import React, { Component } from 'react';
import {token} from "../../App";

import Table from "react-bootstrap/cjs/Table";
import Col from "react-bootstrap/cjs/Col";
import Container from "react-bootstrap/cjs/Container";
import Row from "react-bootstrap/cjs/Row";

class Library extends Component {

  state = {
    title: []
  }

  render() {
    return (
      <>
        <Container>
          <Row>
            <Col>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Titre</th>
                    <th>Durée</th>
                    <th>Artiste</th>
                    <th>Album</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.title.map((title) => {
                    return <tr>
                        <td>{title.name}</td>
                        <td>{title.duration} minutes</td>
                        <td>{title.artist.alias}</td>
                        <td>{title.album.name}</td>
                      </tr>
                  })}
                </tbody>
              </Table>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
  
  componentDidMount() {
    fetch('http://localhost:8080/title', {
      headers: {
        'Authorization': token,
        'Accept'       : 'application/json',
        'Content-Type' : 'application/json',
      },
    })
    .then(res => res.json())
    .then((data) => {
      this.setState({ title: data })
    })
    .catch(console.log)
  }
}

export default Library;
